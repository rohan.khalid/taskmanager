import 'dart:async';
import 'package:task_manager/domain/use_cases/task_usecase.dart';

class TaskBloc {
  final TaskRepository _repository;
  final StreamController<List<Task>> _tasksController =
      StreamController<List<Task>>.broadcast();

  Stream<List<Task>> get tasksStream => _tasksController.stream;

  TaskBloc(this._repository) {
    fetchTasks(); // Automatically fetch tasks when the bloc is created
  }

  Future<void> fetchTasks() async {
    try {
      final tasks = await _repository.getTasks();
      _tasksController.sink.add(tasks);
    } catch (e) {
      // Handle error
      print('Error fetching tasks: $e');
    }
  }

  Future<void> addTask(Task task) async {
    try {
      await _repository.addTask(task);
      fetchTasks(); // Refresh the task list after adding
    } catch (e) {
      // Handle error
      print('Error adding task: $e');
    }
  }

  Future<void> deleteTask(Task task) async {
    try {
      await _repository.deleteTask(task);
      fetchTasks(); // Refresh the task list after deleting
    } catch (e) {
      // Handle error
      print('Error deleting task: $e');
    }
  }

  void dispose() {
    _tasksController.close();
  }
}
