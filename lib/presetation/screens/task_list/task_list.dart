// task_list_screen.dart
import 'package:flutter/material.dart';
import 'package:task_manager/domain/use_cases/task_usecase.dart';
import 'package:task_manager/presetation/bloc/task_bloc/task_bloc.dart';
import 'package:task_manager/widgets/add_task_modal.dart';

class TaskListScreen extends StatelessWidget {
  final TaskBloc bloc;

  const TaskListScreen(this.bloc, {super.key});

  void _openAddTaskModal(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => AddTaskModal(bloc),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: const Text('Task Manager'),
        ),
        body: StreamBuilder<List<Task>>(
          stream: bloc.tasksStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final tasks = snapshot.data!;
              return ListView.builder(
                itemCount: tasks.length,
                itemBuilder: (context, index) {
                  final task = tasks[index];
                  return Card(
                    elevation: 4, // Adjust the elevation as needed
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          12.0), // Adjust the radius for rounded corners
                    ),
                    child: ListTile(
                      title: Text(
                        task.title,
                        style: const TextStyle(
                          fontWeight:
                              FontWeight.w500, // Example: Make the title bold
                        ),
                      ),
                      trailing: IconButton(
                        icon: const Icon(
                          Icons.delete,
                          color: Colors.purple,
                        ),
                        onPressed: () => bloc.deleteTask(task),
                      ),
                    ),
                  );
                },
              );
            } else {
              return const Center(
                child: Text(
                  'This space could use some tasks to brighten it up!',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Colors.purple),
                  textAlign: TextAlign.center,
                ),
              );
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _openAddTaskModal(context),
          child: const Icon(
            Icons.add,
            color: Colors.purple,
          ),
        ),
      ),
    );
  }
}
