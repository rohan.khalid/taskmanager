import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:task_manager/data/repo/task/task_repository.dart';
import 'package:task_manager/presetation/bloc/task_bloc/task_bloc.dart';
import 'package:task_manager/presetation/screens/task_list/task_list.dart';

void main() {
  testWidgets('TaskListScreen UI Test', (WidgetTester tester) async {
    // Create a mock TaskBloc for testing
    final taskRepository = TaskRepositoryImpl();
    final bloc = TaskBloc(
        taskRepository); // Initialize TaskBloc with appropriate dependencies if needed

    // Pump the TaskListScreen widget
    await tester.pumpWidget(MaterialApp(
      home: TaskListScreen(bloc),
    ));

    // Verify that the app bar is rendered correctly
    expect(find.byType(AppBar), findsOneWidget);
    expect(find.text('Task Manager'), findsOneWidget);

    // Verify that the floating action button is rendered correctly
    expect(find.byType(FloatingActionButton), findsOneWidget);
    expect(find.byIcon(Icons.add), findsOneWidget);
  });
}
