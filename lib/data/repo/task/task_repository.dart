// task_repository_impl.dart
import 'package:task_manager/domain/use_cases/task_usecase.dart';

class TaskRepositoryImpl implements TaskRepository {
  List<Task> _tasks = [];

  @override
  Future<List<Task>> getTasks() async {
    return _tasks;
  }

  @override
  Future<void> addTask(Task task) async {
    _tasks.add(task);
  }

  @override
  Future<void> deleteTask(Task task) async {
    _tasks.remove(task);
  }
}
