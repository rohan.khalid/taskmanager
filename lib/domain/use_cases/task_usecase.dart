// task.dart
class Task {
  final String id;
  final String title;

  Task({required this.id, required this.title});
}

// task_repository.dart
abstract class TaskRepository {
  Future<List<Task>> getTasks();
  Future<void> addTask(Task task);
  Future<void> deleteTask(Task task);
}
