import 'dart:async';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:task_manager/domain/use_cases/task_usecase.dart';
import 'package:task_manager/presetation/bloc/task_bloc/task_bloc.dart';

// Mock class for TaskRepository
class MockTaskRepository extends Mock implements TaskRepository {}

void main() {
  group('TaskBloc', () {
    late TaskBloc taskBloc;
    late MockTaskRepository mockRepository;
    late StreamController<List<Task>> controller;

    setUp(() {
      mockRepository = MockTaskRepository();
      controller = StreamController<List<Task>>();

      taskBloc = TaskBloc(mockRepository);
    });

    tearDown(() {
      controller.close();
      taskBloc.dispose();
    });

    test('fetchTasks emits tasks from repository', () async {
      final tasks = [Task(id: '1', title: 'Task 1')];

      // Mock the behavior of getTasks() method in the repository
      when(mockRepository.getTasks()).thenAnswer((_) async => tasks);

      // Listen to the stream and expect tasks to be emitted
      expectLater(taskBloc.tasksStream, emitsInOrder([tasks]));

      // Trigger fetchTasks() method
      await taskBloc.fetchTasks();
    });

    test('addTask calls addTask method in repository', () async {
      final task = Task(id: '1', title: 'New Task');

      // Trigger addTask() method in TaskBloc
      await taskBloc.addTask(task);

      // Verify that addTask() method in repository is called with the task
      verify(mockRepository.addTask(task)).called(1);
    });

    // Add more test cases as needed for deleteTask and error scenarios
    test('deleteTask calls deleteTask method in repository', () async {
      final taskToDelete = Task(id: '1', title: 'Task to Delete');

      // Trigger deleteTask() method in TaskBloc
      await taskBloc.deleteTask(taskToDelete);

      // Verify that deleteTask() method in repository is called with the task to delete
      verify(mockRepository.deleteTask(taskToDelete)).called(1);
    });
  });
}
