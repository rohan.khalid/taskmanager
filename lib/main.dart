import 'package:flutter/material.dart';
import 'package:task_manager/data/repo/task/task_repository.dart';
import 'package:task_manager/domain/use_cases/task_usecase.dart';

import 'package:task_manager/presetation/bloc/task_bloc/task_bloc.dart';
import 'package:task_manager/presetation/screens/task_list/task_list.dart';

void main() {
  final TaskRepository repository = TaskRepositoryImpl() as TaskRepository;
  final TaskBloc bloc = TaskBloc(repository);

  runApp(MyApp(bloc));
}

class MyApp extends StatelessWidget {
  final TaskBloc bloc;
  const MyApp(this.bloc, {super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Task Manager',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        appBarTheme: const AppBarTheme(
          centerTitle: true, // Center the title
          titleTextStyle: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 24,
            color: Colors.purple,
          ),
        ),
      ),
      home: TaskListScreen(bloc),
    );
  }
}
